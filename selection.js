
var tablets = [
    {merk:"apple", kleur:"spacegrey", formaat:7.9, afb: "apple_1.png",
    link:"apple_1.html", tonenMerk:0, tonenKleur:0},

    {merk:"apple", kleur:"goud", formaat:9.7, afb: "apple_2.png",
    link:"apple_2.html", tonenMerk:0, tonenKleur:3},

    {merk:"apple", kleur:"grijs", formaat:12.9, afb: "apple_3.png",
    link:"apple_3.html", tonenMerk:0, tonenKleur:5},

    {merk:"apple", kleur:"zilver", formaat:10.5, afb: "apple_4.png",
    link:"apple_4.html", tonenMerk:0, tonenKleur:0},


    {merk:"huawei", kleur:"grijs", formaat:10.8, afb: "huawei_1.png",
    link:"huawei_1.html", tonenMerk:4, tonenKleur:5},

    {merk:"huawei", kleur:"grijs", formaat:9.6, afb: "huawei_2.png",
    link:"huawei_2.html", tonenMerk:4, tonenKleur:5},

    {merk:"xiaomi", kleur:"grijs", formaat:8, afb: "xiaomi_1.png",
    link:"xiaomi_1.html", tonenMerk:5, tonenKleur:5},

    {merk:"lenovo", kleur:"zwart", formaat:10.1, afb: "lenovo_1.png",
    link:"lenovo_1.html", tonenMerk:2, tonenKleur:1},

    {merk:"microsoft", kleur:"zwart", formaat:10, afb: "microsoft_1.jpeg",
    link:"microsoft_1.html", tonenMerk:3, tonenKleur:0},

    {merk:"samsung", kleur:"zwart", formaat:10.5, afb: "samsung_1.jpeg",
    link:"samsung_1.html", tonenMerk:1, tonenKleur:0},

    {merk:"samsung", kleur:"grijs", formaat:10.5, afb: "samsung_2.jpeg",
    link:"samsung_2.html", tonenMerk:1, tonenKleur:5},

];
//hier nog 2 tablet objecten toevoegen

var merken = ["apple", "samsung", "lenovo", "microsoft", "huawei", "xiaomi"];

var kleuren = ["zilver", "zwart", "spacegrey", "goud", "wit", "grijs"];

var fKleur = false;
var fMerk = false;
function initialiseer() {
    for (var i=1;i<=11;i++) {
        document.getElementById("img"+i).src = "";
        document.getElementById("img"+i).style.display = "none";
    }
    var tlen =tablets.length;
    for(var i=0;i<tlen;i++) {
        tablets[i].tonenMerk=0;
        tablets[i].tonenKleur=0;
    }
    fKleur = false;
    fMerk = false;

}
function toonTablet(index, pos) {
    document.getElementById("img"+pos).src = "img/tablets/" + tablets[index].afb;
    document.getElementById("img"+pos).style.display = "inline";
    document.getElementById("imgLink"+ pos).href = tablets[index].link;

}
function toonSelectie(){
    initialiseer();
    filterMerk();
    filterKleur();
    var getoondeImg = 1;
    var tlen =tablets.length;
    //bepaal of een tablet getoond wordt.
    for(var i=0;i<tlen;i++) {
        if (fMerk && fKleur) {
            if (tablets[i].tonenMerk == 1 && tablets[i].tonenKleur==1) {
                toonTablet(i,getoondeImg);
                getoondeImg++;
            }
        } else if (fMerk && !fKleur) {
            if (tablets[i].tonenMerk == 1) {
                toonTablet(i,getoondeImg);
                getoondeImg++;
            }
        } else if (!fMerk && fKleur) {
            if (tablets[i].tonenKleur==1) {
                toonTablet(i,getoondeImg);
                getoondeImg++;
            }
        }
        else{ // als er geen selectie is gemaakt, laat dan alles zien
            showAll();
        }
    }
}

function filterMerk() {
    var tlen = tablets.length;
    var mlen = merken.length;
    console.log(tlen);
    for (var m=0;m<mlen;m++) {
        if (document.getElementById("filter-"+ merken[m]).checked) {
            fMerk = true;
            for(var i=0;i<tlen;i++) {
                if (tablets[i].merk == merken[m]) {
                    tablets[i].tonenMerk = 1;
                }
            }
            // merkGevonden = true
        }
    }
}

function filterKleur(){
    var klen = kleuren.length;
    var tlen = tablets.length;
    for (var k=0;k<klen;k++) {
        if (document.getElementById(kleuren[k]).checked) {
            fKleur = true;
            for(var i=0;i<tlen;i++) {
                if (tablets[i].kleur == kleuren[k]){
                    tablets[i].tonenKleur = 1;
                }
            }
        }
    }

}

//laat alle items zien
function showAll(){
    var tlen =tablets.length;
    var getoondeImg = 1;
    for(var i=0;i<tlen;i++) {
        toonTablet(i,getoondeImg);
        getoondeImg++;


    }
}

showAll();//bij volledig laden van de DOM wordt deze functie aangeroepen
